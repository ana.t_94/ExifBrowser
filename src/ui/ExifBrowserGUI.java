/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ui;

import exifbrowser.ExtractDAOSQL;
import exifbrowser.Image;
import exifbrowser.ImageDAOMongo;
import exifbrowser.ImportDataSQL;
import exifbrowser.Metadata;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;

public class ExifBrowserGUI extends javax.swing.JFrame {

    public ExifBrowserGUI() {
        initMenu();
        moveToSelector();
    }                   

    private void jMenuImportActionPerformed(java.awt.event.ActionEvent evt) {                                            
        this.jDialog.setVisible(true);
    }                                           

    private void jDialogMouseClicked(java.awt.event.MouseEvent evt) {                                     
    }                                    

    private void jMenuImportMouseClicked(java.awt.event.MouseEvent evt) {                                         

    }                                        

    private void jMenuImportMenuSelected(javax.swing.event.MenuEvent evt) {                                         
        this.jDialog.setVisible(true);
    }                                        

    private void jFileChooser1ActionPerformed(java.awt.event.ActionEvent evt) {                                              

        switch (evt.getActionCommand()) {
            case JFileChooser.CANCEL_SELECTION:
                this.jDialog.setVisible(false);
                break;
            case JFileChooser.APPROVE_SELECTION: {
                try {
                    String[] systemPathArray = this.jFileChooser1.getSelectedFile().getAbsolutePath().split("\\.");
                    String extension = systemPathArray[systemPathArray.length - 1];
                    if (extension.equals("txt")) {
                        ImportDataSQL.readFile(this.jFileChooser1.getSelectedFile().getAbsolutePath());
                        List<Image> images = ExtractDAOSQL.extractImages();
                        ImageDAOMongo imageDAO = new ImageDAOMongo();

                        for (Image image : images) {
                            imageDAO.insert(image);
                        }
                        this.jDialog.setVisible(false);
                        this.moveToSelector();
                    } else {
                        System.err.println("El archivo con path: " + this.jFileChooser1.getSelectedFile().getAbsolutePath() + " no es valido");
                    }
                } catch (Exception ex) {
                    System.err.println("El archivo con path: " + this.jFileChooser1.getSelectedFile().getAbsolutePath() + " no es valido");
                    ex.printStackTrace();
                }
            }
            break;
        }
    }                                             

    private void jButtonViewBackActionPerformed(java.awt.event.ActionEvent evt) {                                                
        this.moveToSelector();
    }                                               

    private void jButtonSelectorSearchActionPerformed(java.awt.event.ActionEvent evt) {                                                      
        this.moveToSelector();
    }                                                     

    private void jButtonSelectorViewActionPerformed(java.awt.event.ActionEvent evt) {                                                    
        this.indexImage = this.jTableSelector.getSelectedRow();
        if(indexImage != -1)
        this.moveToView();
    }                                                   

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {                                            
        
    }                                           

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {                                           
        this.moveToView();
    }                                          

    private void jComboBoxSelectorActionPerformed(java.awt.event.ActionEvent evt) {                                                  

    }                                                 

    public void moveToSelector() {
        this.jComboBox1 = null;
        this.getContentPane().removeAll();
        this.initSelectorPanel();
        this.setContentPane(this.selector);
        this.invalidate();
        this.validate();
        this.setVisible(true);
    }

    private void initSelectorPanel() {
        String selectedFilter = this.jComboBoxSelector == null ? "name" : ((String) this.jComboBoxSelector.getSelectedItem());
        String search = this.jTextFieldSelectorSearch == null ? "" : this.jTextFieldSelectorSearch.getText().trim();
        images = daoMongo.getImagesFilter(selectedFilter.toLowerCase(), search);
        selector = new javax.swing.JPanel();
        jLabelSelector = new javax.swing.JLabel();
        jTextFieldSelectorSearch = new javax.swing.JTextField();
        jComboBoxSelector = new javax.swing.JComboBox<>();
        jButtonSelectorSearch = new javax.swing.JButton();
        jButtonSelectorView = new javax.swing.JButton();
        jScrollPaneSelector = new javax.swing.JScrollPane();
        jTableSelector = new javax.swing.JTable();
        String[] head = {"Name", "Extension", "Size", "Path"};

        String[][] table = null;

        if (this.images.size() > 0) {
            table = new String[this.images.size()][head.length];
        }

        int i = 0;
        for (Image image : this.images) {
            table[i][0] = image.getName();
            table[i][1] = image.getExtension();
            table[i][2] = image.getSize();
            table[i][3] = image.getPath();
            i++;
        }

        if (this.images.size() > 0) {
            javax.swing.table.TableModel model = new javax.swing.table.DefaultTableModel(table, head) {
                public boolean isCellEditable(int row, int column) {
                    return false;//This causes all cells to be not editable
                }
            };
            this.jTableSelector.setModel(model);
        }
        this.jTableSelector.setColumnSelectionAllowed(false);
        this.jTableSelector.setDragEnabled(false);
        this.jTableSelector.setRowSelectionAllowed(true);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        selector.setBackground(new java.awt.Color(0, 102, 102));
        selector.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 102)));

        jLabelSelector.setFont(new java.awt.Font("Lucida Grande", 1, 48)); // NOI18N
        jLabelSelector.setForeground(new java.awt.Color(255, 255, 255));
        jLabelSelector.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelSelector.setText("EXIF BROWSER");

        jComboBoxSelector.setModel(new javax.swing.DefaultComboBoxModel<>(new String[]{"Path", "Name", "Size", "Extension"}));

        jButtonSelectorSearch.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jButtonSelectorSearch.setText("SEARCH");
        jButtonSelectorSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSelectorSearchActionPerformed(evt);
            }
        });

        jButtonSelectorView.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jButtonSelectorView.setText("VIEW");
        jButtonSelectorView.setMaximumSize(new java.awt.Dimension(97, 29));
        jButtonSelectorView.setMinimumSize(new java.awt.Dimension(97, 29));
        jButtonSelectorView.setPreferredSize(new java.awt.Dimension(97, 29));
        jButtonSelectorView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSelectorViewActionPerformed(evt);
            }
        });

        jScrollPaneSelector.setViewportView(jTableSelector);

        javax.swing.GroupLayout selectorLayout = new javax.swing.GroupLayout(selector);
        selector.setLayout(selectorLayout);
        selectorLayout.setHorizontalGroup(
                selectorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, selectorLayout.createSequentialGroup()
                                .addContainerGap(62, Short.MAX_VALUE)
                                .addGroup(selectorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addGroup(selectorLayout.createSequentialGroup()
                                                .addComponent(jTextFieldSelectorSearch)
                                                .addGap(18, 18, 18)
                                                .addComponent(jComboBoxSelector, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(jButtonSelectorSearch)
                                                .addGap(18, 18, 18)
                                                .addComponent(jButtonSelectorView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jLabelSelector, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jScrollPaneSelector, javax.swing.GroupLayout.DEFAULT_SIZE, 564, Short.MAX_VALUE))
                                .addGap(52, 52, 52))
        );
        selectorLayout.setVerticalGroup(
                selectorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(selectorLayout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addComponent(jLabelSelector)
                                .addGap(18, 18, 18)
                                .addGroup(selectorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jTextFieldSelectorSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jComboBoxSelector, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jButtonSelectorSearch)
                                        .addComponent(jButtonSelectorView, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPaneSelector, javax.swing.GroupLayout.PREFERRED_SIZE, 498, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(62, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(selector, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(selector, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        this.jTextFieldSelectorSearch.setText(search);
        this.jComboBoxSelector.setSelectedItem(selectedFilter);
        pack();
    }

    private void moveToView() {
        this.getContentPane().removeAll();
        this.initViewPanel();
        this.setContentPane(this.view);
        this.invalidate();
        this.validate();
        this.setVisible(true);
    }

    private void initViewPanel() {
        String selectedTag = this.jComboBox1 == null ? "" : ((String) this.jComboBox1.getSelectedItem());
        view = new javax.swing.JPanel();
        jButtonViewBack = new javax.swing.JButton();
        jComboBox1 = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        
        String[] head = {"Tag", "Value"};
        
        metadatas = this.daoMongo.getMetadatasByImageFilter(this.images.get(this.indexImage).getId(), selectedTag);
        
        List<String> filter =this.daoMongo.getMetadatasTagsByImage(this.images.get(this.indexImage).getId());
        
        jComboBox1.addItem("");
        int i =1;
        filter.sort(String.CASE_INSENSITIVE_ORDER);
        for(String str: filter){
           jComboBox1.addItem(str);
        }
        
        this.jComboBox1.setSelectedItem(selectedTag);
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });
        
        String[][] table = null;

        if (this.metadatas.size() > 0) {
            table = new String[this.metadatas.size()][head.length];
        }

        i = 0;
        for (Metadata metadata : this.metadatas) {
            table[i][0] = metadata.getTag();
            table[i][1] = metadata.getValue();
            i++;
        }

        if (this.metadatas.size() > 0) {
            javax.swing.table.TableModel model = new javax.swing.table.DefaultTableModel(table, head) {
                public boolean isCellEditable(int row, int column) {
                    return false;//This causes all cells to be not editable
                }
            };
            this.jTable1.setModel(model);
        }
        this.jTable1.setColumnSelectionAllowed(false);
        this.jTable1.setDragEnabled(false);
        this.jTable1.setRowSelectionAllowed(true);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        view.setBackground(new java.awt.Color(0, 102, 102));
        view.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 51, 102)));
        view.setMaximumSize(new java.awt.Dimension(680, 690));
        view.setMinimumSize(new java.awt.Dimension(680, 690));

        jButtonViewBack.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jButtonViewBack.setText("BACK");
        jButtonViewBack.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButtonViewBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonViewBackActionPerformed(evt);
            }
        });

        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout viewLayout = new javax.swing.GroupLayout(view);
        view.setLayout(viewLayout);
        viewLayout.setHorizontalGroup(
                viewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, viewLayout.createSequentialGroup()
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(viewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(viewLayout.createSequentialGroup()
                                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 587, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addContainerGap())
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, viewLayout.createSequentialGroup()
                                                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 461, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(47, 47, 47)
                                                .addComponent(jButtonViewBack)
                                                .addGap(41, 41, 41))))
        );
        viewLayout.setVerticalGroup(
                viewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(viewLayout.createSequentialGroup()
                                .addGap(22, 22, 22)
                                .addGroup(viewLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButtonViewBack)
                                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 591, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap(28, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 680, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(view, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 699, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(view, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
        );
        
        pack();
    }

    private void initMenu() {
        jDialog = new javax.swing.JDialog();
        jFileChooser1 = new javax.swing.JFileChooser();
        jMenuBar = new javax.swing.JMenuBar();
        jMenuImport = new javax.swing.JMenu();

        jDialog.setMinimumSize(new java.awt.Dimension(570, 360));
        jDialog.setResizable(false);
        jDialog.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jDialogMouseClicked(evt);
            }
        });

        jFileChooser1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jFileChooser1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jDialogLayout = new javax.swing.GroupLayout(jDialog.getContentPane());
        jDialog.getContentPane().setLayout(jDialogLayout);
        jDialogLayout.setHorizontalGroup(
                jDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jDialogLayout.createSequentialGroup()
                                .addComponent(jFileChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
        );
        jDialogLayout.setVerticalGroup(
                jDialogLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jDialogLayout.createSequentialGroup()
                                .addComponent(jFileChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jMenuImport.setText("Import");
        jMenuImport.addMenuListener(new javax.swing.event.MenuListener() {
            public void menuSelected(javax.swing.event.MenuEvent evt) {
                jMenuImportMenuSelected(evt);
            }

            public void menuDeselected(javax.swing.event.MenuEvent evt) {
            }

            public void menuCanceled(javax.swing.event.MenuEvent evt) {
            }
        });
        jMenuImport.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenuImportMouseClicked(evt);
            }
        });
        jMenuImport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuImportActionPerformed(evt);
            }
        });
        jMenuBar.add(jMenuImport);

        setJMenuBar(jMenuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 680, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 678, Short.MAX_VALUE)
        );

        pack();
    }

    public static void main(String args[]) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ExifBrowserGUI().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify                     
    private javax.swing.JButton jButtonSelectorSearch;
    private javax.swing.JButton jButtonSelectorView;
    private javax.swing.JButton jButtonViewBack;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBoxSelector;
    private javax.swing.JDialog jDialog;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JLabel jLabelSelector;
    private javax.swing.JMenuBar jMenuBar;
    private javax.swing.JMenu jMenuImport;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPaneSelector;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTableSelector;
    private javax.swing.JTextField jTextFieldSelectorSearch;
    private javax.swing.JPanel selector;
    private javax.swing.JPanel view;
    // End of variables declaration                   
    List<Image> images = new ArrayList<>();
    List<Metadata> metadatas = new ArrayList<>();
    ImageDAOMongo daoMongo = new ImageDAOMongo();
    int indexImage = -1;
}