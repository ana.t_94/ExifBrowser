package exifbrowser;

import java.util.List;

public class Image {

    private String path;
    private List<Metadata> metadatas;
    private String name;
    private String extension;
    private String size;
    private long id;

    public Image(long id, String path, String name, String extension, String size, List<Metadata> metadatas) {
        this.path = path;
        this.name = name;
        this.extension = extension;
        this.size = size;
        this.metadatas = metadatas;
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<Metadata> getMetadatas() {
        return metadatas;
    }

    public void setMetadatas(List<Metadata> metadatas) {
        this.metadatas = metadatas;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Metadata> getMetadataList() {
        return metadatas;
    }

    public void addMetadata(Metadata metadata) {
        this.metadatas.add(metadata);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "Image{" + "path=" + path + ", name=" + name + ", extension=" + extension + ", size=" + size + '}';
    }
}
