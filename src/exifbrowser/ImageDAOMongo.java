package exifbrowser;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONArray;
import org.json.JSONObject;

public class ImageDAOMongo {

    public void insert(Image image) {
        MongoCollection<Document> collection = ConnectionDBNoSQL.getConnectionDBNoSQL().getCollection();
        BasicDBObject findFilter = new BasicDBObject();
        findFilter.put("_id", image.getId());

        MongoCursor<Document> cursor = collection.find(findFilter).iterator();

        JSONObject document = null;
        JSONArray metadatas = new JSONArray();

        for (Metadata metadata : image.getMetadataList()) {
            JSONObject documentMetadata = new JSONObject();
            documentMetadata.put("tag", metadata.getTag());
            documentMetadata.put("value", metadata.getValue());
            metadatas.put(documentMetadata);
        }

        document = new JSONObject();
        document.put("extension", image.getExtension());
        document.put("metadatas", metadatas);
        document.put("name", image.getName());
        document.put("path", image.getPath());
        document.put("size", image.getSize());

        if (!cursor.hasNext()) {
            document.put("_id", image.getId());
            collection.insertOne(Document.parse(document.toString()));
        }else{
            collection.replaceOne(Filters.eq("_id", image.getId()), Document.parse(document.toString()));
        }
    }

    public void deleteAll() {
        MongoCollection<Document> collection = ConnectionDBNoSQL.getConnectionDBNoSQL().getCollection();
        collection.drop();
    }

    public List<Image> getImagesFilter(String key, String value) {
        //db.images.find({"key": /.*value.*/},{"metadatas": 0})
        List<Image> images = new ArrayList<>();
        MongoCollection<Document> collection = ConnectionDBNoSQL.getConnectionDBNoSQL().getCollection();
        BasicDBObject regexQuery = new BasicDBObject();
        BasicDBObject projection = new BasicDBObject();

        regexQuery.put(key, new BasicDBObject("$regex", ".*" + value + ".*"));
        projection.put("metadatas", 0);
        MongoCursor<Document> cursor = collection.find(regexQuery).projection(projection).iterator();
        while (cursor.hasNext()) {
            JSONObject json = new JSONObject(cursor.next().toJson());
            images.add(new Image(json.getLong("_id"), json.getString("path"), json.getString("name"), json.getString("extension"), json.getString("size"), null));
        }

        cursor.close();

        return images;
    }

    public List<Metadata> getMetadatasByImageFilter(long idImage, String filter) {
        //db.images.aggregate([
        //  { "$unwind": "$metadatas" },
        //  { "$match": { "metadatas.tag": /.*Exif.*/, "_id" : 46546 } },
        //  { "$group": {
        //      "_id": "$_id",
        //      "metadatas": { "$push": "$metadatas" }
        //  }}
        //  ])

        List<Metadata> metadatas = new ArrayList<>();
        MongoCollection<Document> collection = ConnectionDBNoSQL.getConnectionDBNoSQL().getCollection();
        List<Bson> query = new ArrayList<>();

        BasicDBObject match = new BasicDBObject();
        BasicDBObject regexQuery = new BasicDBObject();
        regexQuery.put("metadatas.tag", new BasicDBObject("$regex", ".*" + filter + ".*"));
        regexQuery.put("_id", idImage);
        match.put("$match", regexQuery);

        BasicDBObject unwind = new BasicDBObject();
        unwind.put("$unwind", "$metadatas");

        BasicDBObject group = new BasicDBObject();
        BasicDBObject groupCondition = new BasicDBObject();
        groupCondition.put("_id", "$_id");

        BasicDBObject push = new BasicDBObject();
        push.put("$push", "$metadatas");
        groupCondition.put("metadatas", push);
        group.put("$group", groupCondition);

        query.add(unwind);
        query.add(match);
        query.add(group);

        MongoCursor<Document> cursor = collection.aggregate(query).iterator();
        if (cursor.hasNext()) {
            metadatas = jsonToMetadata(new JSONObject(cursor.next().toJson()));
        }

        cursor.close();

        return metadatas;
    }

    public List<String> getMetadatasTagsByImage(long idImage) {
        //db.images.find({"_id": 786786},{"metadatas.tag": 1});
        List<String> metadatas = new ArrayList<>();
        MongoCollection<Document> collection = ConnectionDBNoSQL.getConnectionDBNoSQL().getCollection();

        BasicDBObject filter = new BasicDBObject();
        BasicDBObject projection = new BasicDBObject();

        filter.put("_id", idImage);
        projection.put("metadatas.tag", 1);

        MongoCursor<Document> cursor = collection.find(filter).projection(projection).iterator();
        if (cursor.hasNext()) {
            JSONObject json = new JSONObject(cursor.next().toJson());
            JSONArray metadatasJson = json.getJSONArray("metadatas");
            for (int i = 0; i < metadatasJson.length(); i++) {
                metadatas.add(metadatasJson.getJSONObject(i).getString("tag"));
            }

        }

        cursor.close();

        return metadatas;
    }

    private List<Metadata> jsonToMetadata(JSONObject json) {
        List<Metadata> metadatas = new ArrayList<>();
        JSONArray metadatasJson = json.getJSONArray("metadatas");

        for (int i = 0; i < metadatasJson.length(); i++) {
            metadatas.add(new Metadata(metadatasJson.getJSONObject(i).getString("tag"), (metadatasJson.getJSONObject(i).has("value") ? metadatasJson.getJSONObject(i).getString("value") : "")));
        }
        return metadatas;
    }
}
