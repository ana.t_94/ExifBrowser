package exifbrowser;

import com.drew.imaging.jpeg.JpegMetadataReader;
import com.drew.imaging.jpeg.JpegProcessingException;
import com.drew.imaging.jpeg.JpegSegmentMetadataReader;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import com.drew.metadata.exif.ExifReader;
import java.io.BufferedReader;

import java.io.IOException;
import java.util.Arrays;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ImportDataSQL {

    public static final String TYPE_FILE = "file";

    public static void extractMetaData(String path, Connection connection) throws JpegProcessingException, IOException, SQLException, SQLException, StringIndexOutOfBoundsException {
        String[] systemPathArray = path.split("/");
        String systemPath = path.substring(0, path.length() - systemPathArray[systemPathArray.length - 1].length() - 1);

        File file = new File(path);
        if (file.exists()) {
            Iterable<JpegSegmentMetadataReader> readers = Arrays.asList(new ExifReader());
            Metadata metadataReader = JpegMetadataReader.readMetadata(file, readers);
            Map<String, Tag> fileTags = new HashMap<>();
            List<Tag> metadata = new ArrayList<>();
            Long idImage = 0l;

            for (Directory directory : metadataReader.getDirectories()) {

                for (Tag tag : directory.getTags()) {
                    //System.out.println(tag);
                    if (tag.getDirectoryName().equalsIgnoreCase(TYPE_FILE)) {
                        fileTags.put(tag.getTagName(), tag);
                    } else {
                        metadata.add(tag);
                    }
                }
            }

            String[] pathFile = fileTags.get("File Name").getDescription().split("\\.");

            idImage = insertImage(connection, systemPath, pathFile[0], pathFile[1], fileTags.get("File Size").getDescription());

            for (Tag tag : metadata) {
                if (!tag.getDirectoryName().equalsIgnoreCase(TYPE_FILE)) {
                    insertMetadata(connection, idImage, tag.getDirectoryName(), tag.getTagName(), tag.getDescription());
                }
            }
        } else {
            System.err.println("El archivo con path: " + path + " no existe");
        }
    }

    public static long insertImage(Connection connection, String path, String fileName, String extension, String size) throws SQLException {
        Long idImage = 0l;

        CallableStatement call = connection.prepareCall("{ ? = call INSERTIMAGE(?,?,?,?) }");
        call.registerOutParameter(1, oracle.jdbc.OracleType.NUMBER);
        call.setString(2, path);
        call.setString(3, fileName);
        call.setString(4, extension);
        call.setString(5, size);
        call.executeUpdate();

        idImage = call.getLong(1);
        call.close();

        return idImage;
    }

    public static void insertMetadata(Connection connection, long idFile, String directory, String tag, String value) throws SQLException {
        CallableStatement call = connection.prepareCall("{ call INSERTMETADATA(?,?,?,?) }");
        call.setLong(1, idFile);
        call.setString(2, directory);
        call.setString(3, tag);
        call.setString(4, value);
        call.executeUpdate();
        call.close();
    }

    public static void readFile(String filename) throws FileNotFoundException, IOException, Exception {
        String line = "";
        BufferedReader f = null;
        Connection connection = ConnectionDBSQL.getConnectionDBSQL().getConnection();

        f = new BufferedReader(new FileReader(filename));
        while (((line = f.readLine()) != null)) {

            try {
                extractMetaData(line, connection);
            } catch (JpegProcessingException ex) {
                System.err.println("El archivo con path: " + line + " tiene un formato no válido");
            } catch (IOException ex) {
                System.err.println("El archivo con path: " + line + " no existe");
            } catch (SQLException ex) {
                System.err.println("Error en la insersión o actualización en la base de datos del archivo: " + line);
                ex.printStackTrace();
            } finally {
                continue;
            }

        }

        f.close();
        connection.close();

    }
}
