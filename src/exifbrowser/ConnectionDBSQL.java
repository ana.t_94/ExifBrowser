package exifbrowser;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Handles a Oracle Database Connection
 */
public class ConnectionDBSQL {

    private static ConnectionDBSQL instance;  // The connection instance, unique during execution
    private static Connection connection;         // The connection to the database

    /**
     * Creates a new connection to the database
     */
    private ConnectionDBSQL() {
        connection = null;
        ConnectionDBSQL.reconnect();
    }

    /**
     * Get the connection instance to the database.
     *
     * @return The connection instance to the database
     */
    public static ConnectionDBSQL getConnectionDBSQL() {
        try {
            if (ConnectionDBSQL.instance == null) {
                instance = new ConnectionDBSQL();
            } else if (connection.isClosed()) {
                reconnect();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionDBSQL.class.getName()).log(Level.SEVERE, null, ex);
        }

        return instance;
    }

    /**
     * Creates a new connection to the database
     */
    private static void reconnect() {
        try {
            String driverName = "oracle.jdbc.driver.OracleDriver";
            Class.forName(driverName);
            String serverName = "olimpia.lcc.uma.es";
            String serverPort = "1521";
            String sid = "edgar";
            String username = "INFTEL17_2";
            String password = "INFTEL";
            String url = "jdbc:oracle:thin:@" + serverName + ":" + serverPort + ":" + sid;

            connection = DriverManager.getConnection(url, username, password);

            System.err.println("Succesfully connection");
        } catch (ClassNotFoundException e) {
            System.err.println("Not class found: " + e.getMessage());
        } catch (SQLException e) {
            System.err.println("Connection Error: " + e.getMessage());
        }
    }

    /**
     * Get the connection to the database.
     *
     * @return The connection to the database
     */
    public Connection getConnection() {
        return connection;
    }
    
    public static void close() throws SQLException{
        connection.close();
    }

}
