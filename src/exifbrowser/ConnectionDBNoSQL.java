package exifbrowser;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class ConnectionDBNoSQL {

    private static MongoCollection<Document> collection;
    private static ConnectionDBNoSQL instance;

    /**
     * Creates a new connection to the database
     */
    private ConnectionDBNoSQL() {
        collection = null;
        ConnectionDBNoSQL.reconnect();
    }

    /**
     * Get the connection instance to the database.
     *
     * @return The connection instance to the database
     */
    public static ConnectionDBNoSQL getConnectionDBNoSQL() {

        if (ConnectionDBNoSQL.instance == null) {
            instance = new ConnectionDBNoSQL();
            //} else if (instance.connection.isClosed()) {
            //    reconnect();
        }

        return instance;
    }

    private static void reconnect() {
        MongoClient client = new MongoClient("127.0.0.1", 27017);
        MongoDatabase database = client.getDatabase("test");
        collection = database.getCollection("images_ana_manuel");
    }
    
    /**
     * Get the connection to the database.
     *
     * @return The connection to the database
     */
    public MongoCollection<Document> getCollection() {
        return collection;
    }
}
