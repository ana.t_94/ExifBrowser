package exifbrowser;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ExtractDAOSQL {

    static public List<Metadata> extractMetadatas(long id_image) throws SQLException {
        List<Metadata> metadatas = new ArrayList<>();

        String query = "SELECT t.name, m.value from Metadatas m, Tags t WHERE t.id_tag=m.id_tag AND m.id_image=?";

        PreparedStatement statement = ConnectionDBSQL.getConnectionDBSQL().getConnection().prepareStatement(query);

        statement.setLong(1, id_image);

        ResultSet result = statement.executeQuery();

        while (result.next()) {
            metadatas.add(new Metadata(result.getString("name"), result.getString("value")));
        }
        statement.close();
        return metadatas;
    }

    static public List<Image> extractImages() throws SQLException {
        List<Image> images = new ArrayList<>();

        String query = "SELECT i.id_image, i.name, i.size_, i.extension, p.name AS path FROM IMAGES i, PATHS p WHERE i.id_path=p.id_path";

        PreparedStatement statement = ConnectionDBSQL.getConnectionDBSQL().getConnection().prepareStatement(query);

        ResultSet result = statement.executeQuery();

        while (result.next()) {
            images.add(new Image(result.getLong("id_image"),result.getString("path"), result.getString("name"), result.getString("extension"), result.getString("size_"), extractMetadatas(result.getLong("id_image"))));
        }
        statement.close();
        return images;
    }

}
