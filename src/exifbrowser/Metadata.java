package exifbrowser;


public class Metadata {
	private String value;
	private String tag;

	public Metadata(String tag, String value) {
		this.value = value;
		this.tag = tag;
	}
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	@Override
	public String toString() {
		return "Metadata{" + "tag=" + tag + ", value=" + value + '}';
	}
}
